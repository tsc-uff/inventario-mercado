# Inventário Mercado

## Programação Orientendada a Objetos - Java

### Curso de Tecnologia em Sistemas de Computação Disciplina: Programação Orientada a Objetos AD2 – 2° semestre de 2023.

Você foi contratado(a) para escrever um programa para uma grande rede de supermercados do estado do Rio de Janeiro. O problema que tal rede quer que você resolva utilizando Java básico e os conhecimentos adquiridos na disciplina de POO (sem banco de dados ou similares) é a consulta ao inventário de produtos existentes nas lojas.
Mais especificamente, dados arquivos texto contendo as listagens de produtos e quantidades existentes em cada loja, a rede de supermercados está interessada em informar o nome parcial de algum produto, obter a quantidade total de itens com esse nome em cada loja e o somatório dessas quantidades considerando todas as lojas.
Como exemplo, assuma que existem três lojas e que os inventários das lojas estão nos arquivos “loja1.txt”, “loja2.txt” e “loja3.txt”. A primeira linha do arquivo identifica a loja e as linhas seguintes identificam o nome do produto e a quantidade (valor inteiro), separados por ponto e vírgula:

- Arquivo “loja1.txt”
- Loja Niterói
- Creme de leite;50
- Margarina;60
- Vassoura;78
- Leite;9

- Arquivo “loja2.txt”
- Loja São Gonçalo
- Manga;42
- Leite;91
- Arroz branco;65

- Arquivo “loja3.txt”
- Loja Maricá
- Frango congelado;71
- Vassoura;20
- Margarina;4

A interface do seu programa deverá corresponder à seguinte interação (em azul estão os textos digitados pelo(a) usuário):
encerrar: loja1.txt encerrar: loja2.txt encerrar: loja3.txt encerrar: fim
Seja bem-vindo(a)!
Informe um arquivo

Informe um produto, ou “0” para encerrar: leite

59 unidades em “Loja Niterói”
91 unidades em “Loja São Gonçalo”
0 unidades em “Loja Maricá”
150 unidades no total
Informe um produto, ou “0” para encerrar: vassoura 78 unidades em “Loja Niterói”
0 unidades em “Loja São Gonçalo”
20 unidades em “Loja Maricá”
98 unidades no total
Informe um produto, ou “0” para encerrar: 0
Você deve criar a classe Loja contendo o nome da loja e a coleção de produtos informados no inventário. O construtor da classe recebe o nome do arquivo e, dentro dele, é feita a carga das informações. Essa classe deve incluir uma função que, dado o nome
parcial de um produto, retorne a quantidade de itens cujo nome do produto inclui o nome parcial informado (veja como exemplo o termo “leite”).
Também deve ser criada a classe Produto contendo como atributos seu nome e quantidade.
Finalmente, toda e qualquer impressão ou solicitação de dados deve ser feita pela classe principal do seu programa. Será descontado se as classes Loja ou Produto fizerem qualquer tipo de comunicação com o usuário.
Atenção: Seu programa deve executar com quaisquer dados informados como parâmetros de entrada. Se o seu programa resolver somente o exemplo dado no enunciado do exercício então sua solução será totalmente descontada.