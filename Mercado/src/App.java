import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        
        Scanner scanner = new Scanner(System.in);
        List<Loja> lojas = new ArrayList<>();

        System.out.println("Seja bem-vindo(a)!");
        System.out.println("Informe um arquivo (ou 'fim' para encerrar):");
        
        while (true) {
            String nomeArquivo = scanner.nextLine();
            if (nomeArquivo.equals("fim")) {
                break;
            }

            Loja loja = new Loja(nomeArquivo);
            lojas.add(loja);

            System.out.println("Informe um arquivo (ou 'fim' para encerrar):");
        }

        while (true) {
            System.out.println("Informe um produto, ou '0' para encerrar:");
            String produtoParcial = scanner.nextLine();
            if (produtoParcial.equals("0")) {
                break;
            }

            int quantidadeTotal = 0;

            for (Loja loja : lojas) {
                int quantidadeLoja = 0;
                for (Produto produto : loja.getInventario()) {
                    if (produto.getNome().toLowerCase().contains(produtoParcial.toLowerCase())) {
                        quantidadeLoja += produto.getQuantidade();
                    }
                }
                if (quantidadeLoja > 0) {
                    System.out.println(quantidadeLoja + " unidades em \"" + loja.getNomeLoja() + "\"");
                }
                quantidadeTotal += quantidadeLoja;
            }

            System.out.println(quantidadeTotal + " unidades no total");
        }

        scanner.close();
    }
}
