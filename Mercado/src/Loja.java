import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Loja {

    // Cria classe Loja

    private String nomeLoja;
    private List<Produto> inventario;

    // nomeArquivo = path do arquivo
    
    public Loja(String nomeArquivo) {
        this.nomeLoja = "";
        this.inventario = new ArrayList<>();
    // método para leitura do arquivo
    
    try {
        BufferedReader br = new BufferedReader(new FileReader(nomeArquivo));
        String linha;
        boolean primeiraLinha = true;

        while ((linha = br.readLine()) != null) {
            if (primeiraLinha) {
                this.nomeLoja = linha;
                primeiraLinha = false;
            } else {
                String[] partes = linha.split(";");
                if (partes.length == 2) {
                    String nomeProduto = partes[0];
                    int quantidade = Integer.parseInt(partes[1]);
                    inventario.add(new Produto(nomeProduto, quantidade));
                }
            }
        }

        br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getNomeLoja() {
        return nomeLoja;
    }

    public List<Produto> getInventario() {
        return inventario;
    }
}